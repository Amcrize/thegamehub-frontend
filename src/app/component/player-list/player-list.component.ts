import { Component, OnInit } from '@angular/core';
import { PlayersService } from 'src/app/service/players.service';

@Component({
  selector: 'app-player-list',
  templateUrl: './player-list.component.html',
  styleUrls: ['./player-list.component.css']
})
export class PlayerListComponent implements OnInit {

  players : any;

  constructor(private playerService : PlayersService) { }

  ngOnInit(): void {

    let resp = this.playerService.getPlayer();
    resp.subscribe((data)=>this.players=data);
  }

}