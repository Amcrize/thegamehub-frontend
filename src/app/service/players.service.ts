import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import {HttpClient} from '@angular/common/http'
import { Player } from '../model/Player';

@Injectable({
  providedIn: 'root'
})
export class PlayersService {

  constructor(private http: HttpClient) { }

  getPlayer() {

    return this.http.get<Player[]>('http://localhost:9999/getplayers');

  }
}
